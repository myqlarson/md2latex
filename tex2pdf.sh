#!/bin/bash

# Mike Hannibal 4 July 2013 v1.0

# A script to convert a document from LaTeX to PDF.
# Designed principally to deal with APA style sheet documents.
# Can be modified for other types.

# Set up file list.


fileList=$(find . -maxdepth 1 -name "*.tex" -exec basename {} \;)


# Show a menu and ask for input. If the user entered a valid choice,
# then strip the .tex and build the LaTeX and biber commands
# execute the LaTeX and biber commands


filename=" "

while [ "$filename" != "" ]; do
	select filename in $fileList; do
			if [ -n "$filename" ]; then
				# Create the text file
				filename=${filename%%.*}
				realfilename="$PWD""/""$filename"
				order="pdflatex "$filename".tex"
				order2="biber "$filename				
				echo "++++++++++++++++++++++++++++++"
				echo "PDF to be created with the name: "$filename".pdf"
				echo "With the following command:"
				echo "$order"
				echo "++++++++++++++++++++++++++++++"
				echo "PLEASE check immediately below for error messages"
				echo "++++++++++++++++++++++++++++++"
				$order
 				$order2
				$order
				$order2
				$order
	
			fi
			break 2
	done
done
