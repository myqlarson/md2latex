#!/bin/bash

# Mike Hannibal 23 April 2013 v1.0

# Clean up a tex directory after a PDF has been created

rm *.bbl
rm *.bcf
rm *.blg
rm *.log
rm *.run.xml
rm *.ttt
rm *.fls
rm *.fdb_latexmk
rm *.dvi
rm *.out
