#!/bin/bash

# Mike Hannibal 6 March 2013 v1.0

# A script to convert a document from Markdown to LaTeX.
# Designed principally to deal with APA style sheet documents.
# Can be modified for other types.

# Set up variables.


shorttitle=$(cat shorttitle.md)
abstract=$(cat abstract.md)
keywords=$(cat keywords.md)
affiliations=$(cat affiliations.md)

# Scan the PWD and list all the .md files ready to show a menu of them

fileList=$(find . -maxdepth 1 -name "*.md" -exec basename {} \;)


# Show a menu and ask for input. If the user entered a valid choice,
# then strip the .md and build the pandoc command
# execute the pandoc command
# execute the LaTeX and biber commands


filename=" "

while [ "$filename" != "" ]; do
	select filename in $fileList; do
			if [ -n "$filename" ]; then
				# Create the text file
				filename=${filename%%.*}
				realfilename="$PWD""/""$filename"
				order="/usr/local/bin/pandoc -s -f markdown+tex_math_double_backslash --template=apa6template.tex --variable shorttitle="\"$shorttitle\"" --variable abstract="\"$abstract\"" --variable keywords="\"$keywords\"" --variable affiliations="\"$affiliations\"" "$filename".md -o "$filename".tex"
				echo "Converting to .tex for file - $f.md"
				echo $order
				eval "$order"
# 				order="pdflatex "$filename".tex"
# 				order2="biber "$filename				
# 				echo "++++++++++++++++++++++++++++++"
# 				echo "PDF to be created with the name: "$filename".pdf"
# 				echo "With the following command:"
# 				echo "$order"
# 				echo "++++++++++++++++++++++++++++++"
# 				echo "PLEASE check immediately below for error messages"
# 				echo "++++++++++++++++++++++++++++++"
# 				$order
# 				$order2
# 				$order
# 				$order2
# 				$order
	
			fi
			break 2
	done
done
